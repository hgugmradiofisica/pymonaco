###Pymonaco - Library for Elekta Monaco automation

You can find a downloadable zip bundle in the downloads section. It contains python3.4 portable, pymonaco and a test django-based web server that acts as a TPS scheduler.
Using Windows Task Scheduler to run the calculation queue is recommended.


WARNING: The Autoflow setting modifies treatment planning files. Use it at your own risk! Even though we have not found any single case of data corruption, plans calculated with Autoflow should be carefully verified before treatment. We decline any responsiblity regarding the use of pymonaco and/or the autoflow setting.

More documentation is coming.
Thanks