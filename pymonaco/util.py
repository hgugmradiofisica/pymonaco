# -*- coding: utf-8 -*-
"""
Get Monaco ressources information

@author: R. Ayala
"""

import psutil, subprocess, os, requests, shutil, glob
from .settings import SERVER_IP, SERVER_PORT, STATION, FOCALFOLDER
import logging, re 

def monaco_info():
    cpu_usage=[]
    pid =[]
    #[cpu_usage.append(p.cpu_percent()) for p in psutil.process_iter(attrs=['pid', 'name']) if 'Monaco.exe' in p.info['name']]
    #[pid.append(p.info['pid']) for p in psutil.process_iter(attrs=['pid', 'name']) if 'Monaco.exe' in p.info['name']]
    for p in psutil.process_iter():
        pinfo  = p.as_dict(attrs=['pid', 'name', 'username'])
        if 'Monaco.exe' in pinfo['name']:
            cpu_usage.append(p.cpu_percent())
            pid.append(pinfo['pid'])
   
    return pid, cpu_usage

def get_worklist():
    req = requests.get('http://' + SERVER_IP + ':' + SERVER_PORT + '/retrieve/?station=' + STATION, proxies={'http': None, 'https': None})
    data = req.json()
    return data

def open_monaco():
    path = 'C:/Program Files/CMS/Monaco'
    exe_name = 'Monaco.exe'
    proc = subprocess.Popen(os.path.join(path, exe_name), cwd=path)
    return proc.pid


def send_signal(work_id, status):
    requests.get('http://' + SERVER_IP + ':' + SERVER_PORT + '/listen/?id=' + str(work_id) +'&status='+status, proxies={'http': None, 'https': None})

def clean_folder(folder):
    for element in os.listdir(folder):
        if os.path.isdir(os.path.join(folder,element)):
            shutil.rmtree(os.path.join(folder,element))
    os.remove(os.path.join(folder,'dose.1'))
    tel_files = glob.glob(os.path.join(folder, 'tel.1.dose.*'))
    for f in tel_files:
        os.remove(f)

def duplicate_plan(folder, clinic, patient, plan):
    clinic_folder = glob.glob(os.path.join(FOCALFOLDER, os.path.join(folder,'*'+ clinic)))
    pat_folder = glob.glob(os.path.join(clinic_folder[0], '*'+ patient))
    old_plan_path = os.path.join(pat_folder[0], os.path.join(r"plan", plan))
    folder_exists = os.path.isdir(old_plan_path)
    
    if folder_exists:
        plan_folder = os.path.join(pat_folder[0], os.path.join(r"plan", plan))
        n=1
        regex = r"(?s).*?(?=AUTO)"
        if len(re.findall(regex, plan)) > 1:
            plan = re.findall(regex, plan) [0] # PARA NO ACUMULAR AUTO1AUTO1...
        
        plan_name = plan + 'AUTO' + str(n)
        new_plan_folder =  os.path.join(pat_folder[0], os.path.join(r"plan", plan_name))
        while os.path.isdir(new_plan_folder) == True:
            plan_name = plan + 'AUTO' + str(n)
            new_plan_folder =  os.path.join(pat_folder[0], os.path.join(r"plan", plan_name))
            n +=1
        logging.info(new_plan_folder)
        shutil.copytree(plan_folder, new_plan_folder)
        f = open(os.path.join(new_plan_folder, 'plan'))
        plan_lines = f.readlines()
        f.close()
        plan_lines[2] = plan_name + '\n'
        f = open(os.path.join(new_plan_folder, 'plan'),"w")
        f.writelines(plan_lines)
        f.close()
        try:
            clean_folder(new_plan_folder)
        except:
            pass
        logging.info('El nuevo plan se llama: %s', plan_name)
        logging.info('Se encuentra en la carpeta: %s', new_plan_folder)
        return plan_name, new_plan_folder

