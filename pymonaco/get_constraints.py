# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 13:56:53 2018

@author: 71141933N
"""
import re, collections
###GET DATA###_________________________________________________________________
def createdict():
    d = collections.OrderedDict()
    d['REF'] = ""
    d['LAYERING'] = []
    d['VOLUMES'] = []
    d['BEAMS'] = []
    return d


def substr2dict(string):
    lines = string.replace("    ","").split('\n')
    dict1 =collections.OrderedDict()
    for line in lines:
        if line != '' and line !='!':
            l_split = line.split('=')
            if len(l_split)>1:
                dict1[line.split('=')[0]] = line.split('=')[1]

    return dict1

def find_blocks(string):
    regex = r"(?s)(?<=!)(?!END).*?(?=^!END)"
    matches = re.findall(regex, string, re.MULTILINE)
    return matches

def find_subblocks(string):
    regex = r"(?s)(?<=!).*?(?=!END)"
    match_sub = re.sub(regex, '', string)
    match_sub = re.sub(r"!!END", '', match_sub)
    return match_sub

def find_cost_functions(string):
    regex = r"(?s)(?<=!COSTFUNCTION\n).*?(?=!END)"
    matches = re.findall(regex, string, re.MULTILINE)
    return matches

def find_beams(string):
    regex = r"(?s)(?<=!BEAMDEF\n).*?(?=!END)"
    matches = re.findall(regex, string, re.MULTILINE)
    return matches

def find_general_parameters(string):
    regex= (r"(?<=!)(?!BEAMS)(?!BEAMDEF)(?!LAYERING)(?!VOIDEF)"
    "(?!COSTFUNCTION)(?!DOSE_ENGINES)(?!MOD_EQUIPMENT)(?!END).*?(?=\n)")
    matches = re.findall(regex, string, re.MULTILINE)
    return matches

def get_layering(string):
    lines = string.replace("    ","").split('\n')
    dict1 = collections.OrderedDict()
    for line in lines:
        if line != '' and line != 'LAYERING':
            l_split = line.split(':')
            if len(l_split)>1:
                dict1[l_split[0]] = l_split[1]
            else:
                dict1[l_split[0]] = ''
                
    return dict1

def read_hyp(filename):
    f = open(filename, 'r')
    p_txt = f.read()
    f.close()
    d = createdict()
    f = open(filename, 'r')
    d['REF'] = f.readline()
    f.close()
    
    matches = find_blocks(p_txt)
    #LAYERING
    d['LAYERING'] = get_layering(matches[0])
    
    #VOLUMES
    n=0
    for match in matches:
        
        if match.splitlines()[0]=='VOIDEF':
            sub_matches = find_subblocks(match)
            volume = substr2dict(sub_matches)
            cfs = find_cost_functions(match)
            cfs_list = []
            for cf in cfs:
               cfs_list.append(substr2dict(cf)) 
            volume['CFS'] = cfs_list
            d['VOLUMES'].append(volume)
            n+=1
            
        elif match.splitlines()[0]=='DOSE_ENGINES':
            d['DOSE_ENGINES'] = match
            
        elif match.splitlines()[0]=='MOD_EQUIPMENT':
            sub_matches = find_subblocks(match)
            d['MOD_EQUIPMENT'] = substr2dict(sub_matches)
          
        elif match.splitlines()[0]=='BEAMS':   
            beams = find_beams(match)
            beams_list = []
            for b in beams:
               beams_list.append(substr2dict(b)) 
            d['BEAMS'] = beams_list
    
    f=find_general_parameters(p_txt)
    fdict = general_params2dict(f)
    d.update(fdict)  
    return d      

def general_params2dict(matches):
    dict1 = collections.OrderedDict()
   
    for match in matches:
        plist = match.split('    ')
        dict1[plist[0]] = plist[1]
    return dict1

##WRITE DATA___________________________________________________________________      
def write_hyp(filename, dict1):
    f = open(filename, 'w')
    d = dict1
    f.write(d['REF'])
    f.write('!LAYERING\n')
    for k ,val in d['LAYERING'].items():
        if val:
            f.write('    '+k+':'+val +'\n')
        else:
            f.write('    '+k +'\n')
    f.write('!END\n')
    for vol in d['VOLUMES']:
        f.write('!VOIDEF\n')
        for k ,val in vol.items():
            try:
                if val:
                    f.write('    '+k+'='+val +'\n')
            except:
                pass
            
            
        for cf in vol['CFS']:
             f.write('    '+'!COSTFUNCTION\n')
             for k ,val in cf.items():
                 f.write('    '*2+ k+'='+val +'\n')
             
             f.write('    '+'!END\n')   
        f.write('!END\n')        
    
    f.write('!'+d['DOSE_ENGINES'])
    f.write('!END\n')
    f.write('!MOD_EQUIPMENT\n')
    for k ,val in d['MOD_EQUIPMENT'].items():
        if val:
            f.write('    '+k+'='+val +'\n')    
    f.write('!END\n')
    f.write('!BEAMS\n')
    for beam in d['BEAMS']:
        f.write('    '+'!BEAMDEF\n')
        for k ,val in beam.items():
            if val:
                f.write('    '*2+k+'='+val +'\n') 
        f.write('    '+'!END\n') 
    f.write('!END\n')
    for k ,val in d.items():
        try:
            if val and k!='REF' and k!='DOSE_ENGINES':
                f.write('!'+k+'    '+val +'\n')
        except:
            pass
    f.close()


