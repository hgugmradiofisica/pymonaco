# -*- coding: utf-8 -*-
"""
Created on Thu May 24 10:11:44 2018
SETTINGS
"""
import platform

STATION = platform.node()
SERVER_IP ='10.140.27.213'
SERVER_PORT = '80'
FOCALFOLDER = r'\\MONACO1\focaldata'
TARGET_TRESHOLD = 0.95
MAX_ITER_CF = 3
MAX_ARCS = 2

#STANDALONE OR XIO DATABASE:
STANDALONE = False