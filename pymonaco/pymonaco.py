# -*- coding: utf-8 -*-
"""
Interact with Monaco

@author: R. Ayala

"""
import time, logging, ctypes
from .util import monaco_info, open_monaco
from pywinauto import Application


def connect(max_licenses=3):
    pid, cpu_usage = monaco_info()
    if len(pid)< max_licenses:
        pid = open_monaco()
    app = Application(backend='uia').connect(process=pid)
    app.wait_cpu_usage_lower(threshold=0.5, timeout=30, usage_interval=1.0)
    monaco = app.top_window()
    return monaco

def connect_open_instance():
    app = Application(backend='uia').connect(title_re=".*Monaco")
    monaco = app.top_window()
    return monaco

def authorize(monaco, user='test', key='cms'):
    user_dlg = monaco.child_window(title="User Validation", control_type="Window")
    user_dlg.Edit1.type_keys(user)
    user_dlg.Edit2.type_keys(key)
    user_dlg.OK.invoke() 


def open_patient(monaco, folder, clinic, patient):
    monaco.maximize()
    time.sleep(3)
    children = monaco.children()
    if 'Patient Selection' not in children[0].legacy_properties()['Name']:
        monaco.type_keys('^O')
    logging.info('Abriendo paciente, ID: %s',patient)
    pat_dlg = monaco.child_window(title="Patient Selection", control_type="Window")
    try:
        pat_dlg.X.click()
    except:
        pass
    
    folder_item = getattr(pat_dlg.TreeView,folder)
    clinic_item = getattr(folder_item, clinic)
    
    if clinic_item.exists() == False:
        folder_item.double_click_input()
    clinic_item.double_click_input()
    time.sleep(5)
    pat_dlg.wait("ready")
    pat_dlg.Edit.click_input()
    pat_dlg.Edit.type_keys(patient)
    pat_dlg.wait("ready")
    time.sleep(2)
    first_pat = pat_dlg.ListView1
    first_pat.double_click_input()


def close_windows(monaco):
    user32 = ctypes.windll.user32
    screensize = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)
    r_lim = screensize[0]
    for i in range(3):  # intenta cerrar ventanas 3 veces
        children = monaco.children()
        for child in children:
            try:
                child.collapse()
            except:
                pass


        children = monaco.children()
        for child in children:
            try:
                
                if 'Dose Reference Points' in child.legacy_properties()['Name'] or \
                        'Beam Visibility' in child.legacy_properties()['Name'] or \
                        'Isodoses' in child.legacy_properties()['Name'] or \
                        'Workspace' in child.legacy_properties()['Name'] or \
                        'IMRT Constraints' in child.legacy_properties()['Name'] or \
                        'Progress Meter' in child.legacy_properties()['Name'] or \
                        'Prescription' in child.legacy_properties()['Name'] or \
                        'Beams@' in child.legacy_properties()['Name']:

                    child.set_focus()
                    r = child.rectangle()
                    if r.right > r_lim:
                        child.click_input()
                        child.press_mouse_input(coords=(r.right - 300, r.top + 6))
                        child.release_mouse_input(coords=(r.right - 900, r.top + 6))
                        r = child.rectangle()
                    monaco.double_click_input(coords=(r.right - 10, r.top + 15))

            except:
                pass
                

def open_plan(monaco,plan_name):
    logging.info('Abriendo plan: %s',plan_name)
    monaco.wait("ready")
    monaco[plan_name].double_click_input()
    
    
def optimize_plan(monaco):
    monaco.maximize()
    logging.info('Optimizando... ')
    close_windows(monaco)
    children = monaco.children()
    for child in children:
        try:
            if 'Ready' in child.legacy_properties()['Name']:
                ready = child
        except:
            pass
    r = ready.children()[10]
    if r.get_value() != 'Plan Review Activity                                                  ':
        toolbar = monaco.child_window(title="FocalSim", auto_id="RibbonToolBar", control_type="ToolBar")
        planning = toolbar.child_window(auto_id="Planning", control_type="Tab")
        p = planning.wrapper_object()
        p.click_input()
        opt_button = planning.child_window(auto_id="Optimize", control_type="Button")
        opt=opt_button.wrapper_object()
        opt.click_input()
        dialog = False
        while dialog == False: 
            children = monaco.children()
            for child in children:
                try:
                    if child.legacy_properties()['Name'] == 'Monaco':
                        dialog = True
                        OK = monaco.child_window(title="Monaco", control_type="Window") 
                        OK.OK.invoke()
                        #Dialogo de full fluence calculated
                except:
                    pass
            time.sleep(10) 
                
        logging.info('Empezando segmentación... ')
        monaco.maximize()
        p.click_input()
        opt.click_input()
        dialog = False
        while dialog == False: 
            children = monaco.children()
            for child in children:
                try:
                    if child.legacy_properties()['Name'] == 'Monaco':
                        dialog = True
                        OK = monaco.child_window(title="Monaco", control_type="Window") 
                        OK.OK.invoke()
                        #Dialogo de segmentation complete
                except:
                    pass
            time.sleep(10) 
        
        logging.info('Plan calculado')
    else:
        logging.error('Error en plan, puede que ya estuviera calculado')


def calculate_plan(monaco): 
    monaco.maximize()
    close_windows(monaco)
    toolbar = monaco.child_window(title="FocalSim", auto_id="RibbonToolBar", control_type="ToolBar")
    planning = toolbar.child_window(auto_id="Planning", control_type="Tab")
    p = planning.wrapper_object()
    p.click_input()
    #click en el medio del botón calculate
    split = planning.SplitButton3
    s = split.wrapper_object()
    r = split.rectangle()
    coords = (int((r.right+r.left)/2), int(r.top + 1/4*(r.bottom - r.top)))
    monaco.click_input(coords=coords)
    logging.info('Calculando...')
    children = monaco.children()
    for child in children:
        try:
            if 'Ready' in child.legacy_properties()['Name']:
                ready = child
        except:
            pass
    r=ready.children()[9]
    calc_complete = ' '
    while calc_complete != 'Final dose calculation complete                                       ':
        try:
            calc_complete = r.get_value()
        except:
            pass
        time.sleep(5)
    logging.info('Plan calculado')


def check_close(monaco):
    children = monaco.children()
    for child in children:
        try:
            if child.legacy_properties()['Name'] == 'Monaco':
                    OK = monaco.child_window(title="Monaco", control_type="Window") 
                    OK.yes.invoke() # salva el plan por si acaso
        except:
            pass
                
def save_plan(monaco):
    monaco.maximize()
    monaco.type_keys('^S')
    logging.info('Salvando plan')
    
def close_monaco_old(monaco):
    monaco.type_keys('^Q')
    logging.info('Cerrando Monaco')
    try:
        check_close(monaco)
    except:
        pass
    
def close_monaco(monaco):
    monaco.close()
    logging.info('Cerrando Monaco')
    try:
        check_close(monaco)
    except:
        pass
    
def close_patient(monaco):
    logging.info('Cerrando paciente')
    monaco.maximize() 
    toolbar = monaco.child_window(title="FocalSim", auto_id="RibbonToolBar", control_type="ToolBar")
    menu = toolbar.child_window(auto_id="Main", control_type="Menu")
    m = menu.wrapper_object()
    m.click_input()
    b0 = menu.child_window(auto_id="Open Patient", control_type="Button")
    b2 = b0.Button2.wrapper_object()
    b2.click_input()
    
def run_calc(monaco):
    monaco.maximize()
    children = monaco.children()
    for child in children:
        try:
            if 'Ready' in child.legacy_properties()['Name']:
                ready = child
        except:
            pass
    r=ready.children()[9]
    if r.get_value() == 'Press Optimize to begin stage 1                                       ':
        optimize_plan(monaco)
    elif r.get_value() == 'Press Calculate to calculate dose                                     ':
        calculate_plan(monaco)
    else:
        logging.error('Error en plan  Probablemente ya esté calculado ')

def calc_save(data):
    monaco = connect()
    authorize(monaco)
    open_patient(monaco, data['clinic__folder__name'],
                          data['clinic__name'], data['nhist'])
    open_plan(monaco,data['name'])
    run_calc(monaco)
    time.sleep(7)
    save_plan(monaco)
    time.sleep(5)
    close_monaco(monaco)
    
