# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 13:15:50 2018

@author: R. Ayala
"""

import os, glob, logging
from .settings import FOCALFOLDER, TARGET_TRESHOLD, MAX_ITER_CF, MAX_ARCS
from .util import duplicate_plan, clean_folder
from .get_constraints import read_hyp, write_hyp
from . import pymonaco
import numpy as np


def lower_smoothing (hypfile, n):
    dict1 = read_hyp(hypfile)
    if  dict1['GRADEOFSMOOTHING'] =='High' :
        n_smooth = 3
    elif dict1['GRADEOFSMOOTHING'] =='Medium':
        n_smooth = 2
    elif dict1['GRADEOFSMOOTHING'] =='Low':
        n_smooth = 1
    else:
        n_smooth = 0
    
    if (n_smooth - n) > 0 :
        smooth_state = n_smooth - n
    else:
        smooth_state = 0
    states= ['Off','Low','Medium','High']
    dict1['GRADEOFSMOOTHING']= states[smooth_state] 
    write_hyp(hypfile, dict1)  
    logging.info("Fluence smoothing cambiado a %s", states[smooth_state])
    


def increase_arcs (hypfile, n):
    dict1 = read_hyp(hypfile)
    n_arcs = float(dict1['MAXNARCS'])  
    if (n_arcs + n) <= 4 :
        n_arcs = n_arcs + n
    
    dict1['MAXNARCS']= str(n_arcs)
    write_hyp(hypfile, dict1)  
    logging.info("Nº de arcos máximo igual a %s", str(n_arcs))

def evaluate_targets(hyp_dict):
    cov_values = []
    for vol in hyp_dict['VOLUMES']:
        for cf in vol['CFS']:
           if cf['type'] == 'qp':
               cv = float(cf['isoeffect'])/float(cf['isoconstraint'])
               cov_values.append(cv)
    cov_value = min(cov_values)
    logging.info('cv: %s', cov_value)
    return cov_value
           


def optimize_fluence_smoothing(monaco, data):
    clinic_folder = glob.glob(os.path.join(FOCALFOLDER, os.path.join(data['clinic__folder__name'],'*'+ data['clinic__name'])))
    pat_folder = glob.glob(os.path.join(clinic_folder[0], '*'+ data['nhist']))
    old_plan_path = os.path.join(pat_folder[0], os.path.join(r"plan", data['name']))
    old_hypfile = os.path.join(old_plan_path, data['nhist'] + '.hyp')
    dict_old = read_hyp(old_hypfile)
    cv = evaluate_targets(dict_old)
    hyp_dict = dict_old

    n=1
    while cv < TARGET_TRESHOLD and hyp_dict['GRADEOFSMOOTHING'] != 'Off': 
        data['name'], plan_folder = duplicate_plan(data['clinic__folder__name'], data['clinic__name'], data['nhist'], data['name'])
        hypfile = os.path.join(plan_folder, data['nhist'] + '.hyp')
        try:
            clean_folder(plan_folder)
        except:
            pass
        hyp_dict = read_hyp(hypfile)
        lower_smoothing(hypfile, n)
        logging.info('calculando paciente')
        pymonaco.calc_save(monaco, data)
        hyp_dict = read_hyp(hypfile)    
        cv = evaluate_targets(hyp_dict)
        n += 1

def optimize_arc_number(monaco, data):
    clinic_folder = glob.glob(os.path.join(FOCALFOLDER, os.path.join(data['clinic__folder__name'],'*'+ data['clinic__name'])))
    pat_folder = glob.glob(os.path.join(clinic_folder[0], '*'+ data['nhist']))
    old_plan_path = os.path.join(pat_folder[0], os.path.join(r"plan", data['name']))
    old_hypfile = os.path.join(old_plan_path, data['nhist'] + '.hyp')
    dict_old = read_hyp(old_hypfile)
    cv = evaluate_targets(dict_old)
    hyp_dict = dict_old
    n=1   
    while cv < TARGET_TRESHOLD and int(hyp_dict['MAXNARCS']) < MAX_ARCS: 
        data['name'], plan_folder = duplicate_plan(data['clinic__folder__name'], data['clinic__name'], data['nhist'], data['name'])
        hypfile = os.path.join(plan_folder, data['nhist'] + '.hyp')
        hyp_dict = read_hyp(hypfile)
        increase_arcs(hypfile, n)
        logging.info('calculando paciente')
        pymonaco.calc_save(monaco, data)
        hyp_dict = read_hyp(hypfile)    
        cv = evaluate_targets(hyp_dict)
        n += 1


def apply_cf_limits(cf, isoconstraint):
    # Límites de Cost Functions:
    # Límite inferior

    if cf['type'] == 'o_q' and isoconstraint < 0.02:
        isoconstraint = 0.02
    elif cf['type'] == 'conf' and isoconstraint < 0.01:
        isoconstraint = 0.01
    elif cf['type'] == 'se' and isoconstraint < 1:
        isoconstraint = 1
    elif cf['type'] == 'pa' and isoconstraint < 1:
        isoconstraint = 1

    # Límite superior
    elif cf['type'] == 'o_q' and isoconstraint > 10:
        isoconstraint = 10
    elif cf['type'] == 'conf' and isoconstraint > 1:
        isoconstraint = 1
    elif cf['type'] == 'se' and isoconstraint > 200:
        isoconstraint = 200
    elif cf['type'] == 'pa' and isoconstraint > 100:
        isoconstraint = 100

    return isoconstraint


def tighten_cf(hypfile):
    dict1 = read_hyp(hypfile)
    volumes = dict1['VOLUMES']
    targets = np.zeros(len(volumes))
    for idx, vol in enumerate(volumes):
        for cf in vol['CFS']:
            if cf['type'] == 'qp':
                targets[idx] = 1
    for idx, vol in enumerate(volumes):
        if targets[idx] == 0:
            for idxc, cf in enumerate(vol['CFS']):
                isoeffect = float(cf['isoeffect'])
                isoconstraint = float(cf['isoconstraint'])
                relativeimpact = float(cf['relativeimpact'])

                if isoeffect < isoconstraint*1.05 and cf['type'] in ['se', 'pa', 'conf', 'o_q']:
                    isoeffect = min([isoeffect, isoconstraint])
                    if relativeimpact < 0.25:
                        new_isoconstraint = isoeffect * 0.92
                    elif (relativeimpact >= 0.25 and relativeimpact < 0.75):
                        new_isoconstraint = isoeffect * 0.95
                    elif (relativeimpact >= 0.75 and relativeimpact < 1):
                        new_isoconstraint = isoeffect * 0.98
                    else:
                        new_isoconstraint = isoeffect
                    new_isoconstraint = apply_cf_limits(cf, new_isoconstraint)
                    dict1['VOLUMES'][idx]['CFS'][idxc]['isoconstraint'] = str(new_isoconstraint)
                    logging.info("%s del volumen %s ajustado de %f a %f", cf['type'], vol['name'], isoconstraint,
                                 new_isoconstraint)

    write_hyp(hypfile, dict1)


def tighten_cf2(hypfile):
    dict1 = read_hyp(hypfile)
    #dict1['PARETOMODE'] = '1' #PROBANDO PARETO 
    volumes = dict1['VOLUMES']
    targets = np.zeros(len(volumes))
    for idx, vol in enumerate(volumes):
        for cf in vol['CFS']:
            if cf['type'] == 'qp':
                targets[idx] = 1
    for idx, vol in enumerate(volumes):
        if targets[idx] == 0:
            for idxc, cf in enumerate(vol['CFS']):
                isoeffect = float(cf['isoeffect'])
                isoconstraint = float(cf['isoconstraint'])
                relativeimpact = float(cf['relativeimpact'])
                if isoeffect < isoconstraint * 1.05 and cf['type'] in ['conf', 'o_q']:
                    isoeffect = min([isoeffect, isoconstraint])
                    if relativeimpact < 0.125:
                        new_isoconstraint = isoeffect * 0.90
                    elif (relativeimpact >= 0.125 and relativeimpact < 0.25):
                        new_isoconstraint = isoeffect * 0.92
                    elif (relativeimpact >= 0.25 and relativeimpact < 0.5):
                        new_isoconstraint = isoeffect * 0.93
                    elif (relativeimpact >= 0.5 and relativeimpact < 0.75):
                        new_isoconstraint = isoeffect * 0.95
                    elif (relativeimpact >= 0.75 and relativeimpact <= 1):
                        new_isoconstraint = isoeffect * 0.98
                    new_isoconstraint = apply_cf_limits(cf, new_isoconstraint)
                    dict1['VOLUMES'][idx]['CFS'][idxc]['isoconstraint'] = str(new_isoconstraint)
                    logging.info("%s %s |isoconstr: %.2f -> %.2f, isoeff: %.2f, rel impact: %.2f", cf['type'], vol['name'], isoconstraint,
                                 new_isoconstraint, isoeffect, relativeimpact)

                if isoeffect < isoconstraint * 1.05 and cf['type'] in ['pa']:
                    old_isoeffect = isoeffect
                    isoeffect = min([isoeffect, isoconstraint])
                    min_perc = 10
                    if relativeimpact < 0.25:
                        if isoeffect <= min_perc:
                            new_isoconstraint = isoeffect
                        else:
                            new_isoconstraint = isoeffect - 6

                    elif (relativeimpact >= 0.25 and relativeimpact < 0.5):
                        if isoeffect <= min_perc:
                            new_isoconstraint = isoeffect
                        else:
                            new_isoconstraint = isoeffect - 5

                    elif (relativeimpact >= 0.5 and relativeimpact < 0.75):
                        if isoeffect <= min_perc:
                            new_isoconstraint = isoeffect
                        else:
                            new_isoconstraint = isoeffect - 4

                    elif (relativeimpact >= 0.75 and relativeimpact < 0.9):
                        if isoeffect <= min_perc:
                            new_isoconstraint = isoeffect
                        else:
                            new_isoconstraint = isoeffect - 3

                    elif (relativeimpact >= 0.9 and relativeimpact <= 1):
                        if isoeffect <= min_perc:
                            new_isoconstraint = isoeffect
                        else:
                            new_isoconstraint = isoeffect - 2
                    new_isoconstraint = apply_cf_limits(cf, new_isoconstraint)
                    dict1['VOLUMES'][idx]['CFS'][idxc]['isoconstraint'] = str(new_isoconstraint)
                    logging.info("%s %s |isoconstr: %.2f -> %.2f, isoeff: %.2f, rel impact: %.2f", cf['type'], vol['name'], isoconstraint,
                                 new_isoconstraint, old_isoeffect, relativeimpact)

                if isoeffect < isoconstraint and cf['type'] in ['se']:
                    if relativeimpact < 0.25:
                        new_isoconstraint = isoeffect * 0.92
                    elif (relativeimpact >= 0.25 and relativeimpact <= 0.75):
                        new_isoconstraint = isoeffect * 0.95
                    elif (relativeimpact >= 0.75 and relativeimpact <= 1):
                        new_isoconstraint = isoeffect * 0.98
                    new_isoconstraint = apply_cf_limits(cf, new_isoconstraint)
                    dict1['VOLUMES'][idx]['CFS'][idxc]['isoconstraint'] = str(new_isoconstraint)
                    logging.info("%s %s |isoconstr: %.2f -> %.2f, isoeff: %.2f, rel impact: %.2f", cf['type'], vol['name'], isoconstraint,
                                 new_isoconstraint, isoeffect, relativeimpact)

    write_hyp(hypfile, dict1)

def tighten_cf3(hypfile):
    dict1 = read_hyp(hypfile)
    #dict1['PARETOMODE'] = '1' #PROBANDO PARETO 
    volumes = dict1['VOLUMES']
    targets = np.zeros(len(volumes))
    for idx, vol in enumerate(volumes):
        for cf in vol['CFS']:
            if cf['type'] == 'qp':
                targets[idx] = 1
    for idx, vol in enumerate(volumes):
        if targets[idx] == 0:
            for idxc, cf in enumerate(vol['CFS']):
                isoeffect = float(cf['isoeffect'])
                isoconstraint = float(cf['isoconstraint'])
                relativeimpact = float(cf['relativeimpact'])
                weight = float(cf['weight'])
                
                if weight < 200:
                    if isoeffect < isoconstraint * 1.05 and cf['type'] in ['conf', 'o_q']:
                        old_isoeffect = isoeffect
                        isoeffect = min([isoeffect, isoconstraint])
                        if relativeimpact < 0.125:
                            new_isoconstraint = isoeffect * 0.90
                        elif (relativeimpact >= 0.125 and relativeimpact < 0.25):
                            new_isoconstraint = isoeffect * 0.92
                        elif (relativeimpact >= 0.25 and relativeimpact < 0.5):
                            new_isoconstraint = isoeffect * 0.93
                        elif (relativeimpact >= 0.5 and relativeimpact < 0.75):
                            new_isoconstraint = isoeffect * 0.95
                        elif (relativeimpact >= 0.75 and relativeimpact <= 1):
                            new_isoconstraint = isoeffect * 0.98
                        new_isoconstraint = apply_cf_limits(cf, new_isoconstraint)
                        dict1['VOLUMES'][idx]['CFS'][idxc]['isoconstraint'] = str(new_isoconstraint)
                        logging.info("%s %s |isoconstr: %.2f -> %.2f, isoeff: %.2f, rel impact: %.2f", cf['type'], vol['name'], isoconstraint,
                                     new_isoconstraint, old_isoeffect, relativeimpact)
    
                    if isoeffect < isoconstraint * 1.05 and cf['type'] in ['pa']:
                        old_isoeffect = isoeffect
                        isoeffect = min([isoeffect, isoconstraint])
                        min_perc = 10
                        if relativeimpact < 0.25:
                            if isoeffect <= min_perc:
                                new_isoconstraint = isoeffect
                            else:
                                new_isoconstraint = isoeffect - 6
    
                        elif (relativeimpact >= 0.25 and relativeimpact < 0.5):
                            if isoeffect <= min_perc:
                                new_isoconstraint = isoeffect
                            else:
                                new_isoconstraint = isoeffect - 5
    
                        elif (relativeimpact >= 0.5 and relativeimpact < 0.75):
                            if isoeffect <= min_perc:
                                new_isoconstraint = isoeffect
                            else:
                                new_isoconstraint = isoeffect - 4
    
                        elif (relativeimpact >= 0.75 and relativeimpact < 0.9):
                            if isoeffect <= min_perc:
                                new_isoconstraint = isoeffect
                            else:
                                new_isoconstraint = isoeffect - 3
    
                        elif (relativeimpact >= 0.9 and relativeimpact <= 1):
                            if isoeffect <= min_perc:
                                new_isoconstraint = isoeffect
                            else:
                                new_isoconstraint = isoeffect - 2
                        new_isoconstraint = apply_cf_limits(cf, new_isoconstraint)
                        dict1['VOLUMES'][idx]['CFS'][idxc]['isoconstraint'] = str(new_isoconstraint)
                        logging.info("%s %s |isoconstr: %.2f -> %.2f, isoeff: %.2f, rel impact: %.2f", cf['type'], vol['name'], isoconstraint,
                                     new_isoconstraint, old_isoeffect, relativeimpact)
    
                    if isoeffect < isoconstraint and cf['type'] in ['se']:
                        if relativeimpact < 0.25:
                            new_isoconstraint = isoeffect * 0.92
                        elif (relativeimpact >= 0.25 and relativeimpact <= 0.75):
                            new_isoconstraint = isoeffect * 0.95
                        elif (relativeimpact >= 0.75 and relativeimpact <= 1):
                            new_isoconstraint = isoeffect * 0.98
                        new_isoconstraint = apply_cf_limits(cf, new_isoconstraint)
                        dict1['VOLUMES'][idx]['CFS'][idxc]['isoconstraint'] = str(new_isoconstraint)
                        logging.info("%s %s |isoconstr: %.2f -> %.2f, isoeff: %.2f, rel impact: %.2f", cf['type'], vol['name'], isoconstraint,
                                     new_isoconstraint, isoeffect, relativeimpact)

    write_hyp(hypfile, dict1)


def loosen_cf(hypfile):
    dict1 = read_hyp(hypfile)
    volumes = dict1['VOLUMES']
    targets = np.zeros(len(volumes))
    for idx, vol in enumerate(volumes):
        for cf in vol['CFS']:
            if cf['type'] == 'qp':
                targets[idx] = 1
    for idx, vol in enumerate(volumes):
        if targets[idx] == 0:
            for idxc, cf in enumerate(vol['CFS']):
                isoeffect = float(cf['isoeffect'])
                isoconstraint = float(cf['isoconstraint'])
                relativeimpact = float(cf['relativeimpact'])
                if isoeffect > isoconstraint and cf['type'] in ['se', 'pa', 'conf', 'o_q']:
                    new_isoconstraint = isoeffect * 1.05
                    new_isoconstraint = apply_cf_limits(cf, new_isoconstraint)
                    dict1['VOLUMES'][idx]['CFS'][idxc]['isoconstraint'] = str(new_isoconstraint)
                    logging.info("%s %s |isoconstr: %.2f -> %.2f, isoeff: %.2f, rel impact: %.2f", cf['type'], vol['name'], isoconstraint,
                                 new_isoconstraint, isoeffect, relativeimpact)
    write_hyp(hypfile, dict1)


def get_plan_path(data):
    clinic_folder = glob.glob(
        os.path.join(FOCALFOLDER, os.path.join(data['clinic__folder__name'], '*' + data['clinic__name'])))
    pat_folder = glob.glob(os.path.join(clinic_folder[0], '*' + data['nhist']))
    plan_path = os.path.join(pat_folder[0], os.path.join(r"plan", data['name']))
    return plan_path

def optimize_cfs(monaco, data):
    old_plan_path = get_plan_path(data)
    old_hypfile = os.path.join(old_plan_path, data['nhist'] + '.hyp')
    dict_old = read_hyp(old_hypfile)
    logging.info('Optimizando funciones de coste:')
    cv = evaluate_targets(dict_old)
    hyp_dict = dict_old
    n=1   
    while cv > TARGET_TRESHOLD and n <= MAX_ITER_CF:
        data['name'], plan_folder = duplicate_plan(data['clinic__folder__name'], data['clinic__name'], data['nhist'], data['name'])
        hypfile = os.path.join(plan_folder, data['nhist'] + '.hyp')
        hyp_dict = read_hyp(hypfile)
        tighten_cf(hypfile)
        logging.info('calculando plan optimizado ')
        pymonaco.calc_save(monaco, data)
        hyp_dict = read_hyp(hypfile)    
        cv = evaluate_targets(hyp_dict)
        n += 1

def autoflow(data):
    pymonaco.calc_save(data)
    old_plan_path = get_plan_path(data)
    old_hypfile = os.path.join(old_plan_path, data['nhist'] + '.hyp')
    hyp_dict = read_hyp(old_hypfile)
    cv = evaluate_targets(hyp_dict)
    if cv > TARGET_TRESHOLD:
        #FORZAR ISOCONSTRAINTS EN BUCLE:
        n = 1
        while cv > TARGET_TRESHOLD and n <= MAX_ITER_CF:
            data['name'], plan_folder = duplicate_plan(data['clinic__folder__name'], data['clinic__name'],
                                                       data['nhist'], data['name'])
            hypfile = os.path.join(plan_folder, data['nhist'] + '.hyp')
            hyp_dict = read_hyp(hypfile)
            tighten_cf3(hypfile)
            logging.info('calculando plan optimizado ')
            try:
                pymonaco.calc_save( data)
                hyp_dict = read_hyp(hypfile)
                cv = evaluate_targets(hyp_dict)
            except:
                pass
            n += 1

    else:
        #CAMBIAR PARAMETROS A ARCS = 2 y Fluence Smoothing = Low
        if int(hyp_dict['MAXNARCS']) == 1 or hyp_dict['GRADEOFSMOOTHING'] in ['High', 'Medium']:
            if int(hyp_dict['MAXNARCS']) == 1:
                hyp_dict['MAXNARCS'] = '2'
                logging.info('MAXARCS = 2')
            if hyp_dict['GRADEOFSMOOTHING'] in ['High', 'Medium']:
                hyp_dict['GRADEOFSMOOTHING'] = 'Low'
                logging.info('GRADEOFSMOOTHING = Low')
            data['name'], plan_folder = duplicate_plan(data['clinic__folder__name'], data['clinic__name'],
                                                       data['nhist'], data['name'])
            hypfile = os.path.join(plan_folder, data['nhist'] + '.hyp')
            write_hyp(hypfile, hyp_dict)
            logging.info('calculando plan con parámetros de segmentación optimizados ')
            pymonaco.calc_save(data)
            #Evaluamos la nueva cobertura:
            hyp_dict = read_hyp(hypfile)
            cv = evaluate_targets(hyp_dict)

        if cv > TARGET_TRESHOLD:
            # FORZAR ISOCONSTRAINTS SOLO 1 vez
            data['name'], plan_folder = duplicate_plan(data['clinic__folder__name'], data['clinic__name'],
                                                       data['nhist'], data['name'])
            hypfile = os.path.join(plan_folder, data['nhist'] + '.hyp')
            tighten_cf3(hypfile)
            logging.info('calculando plan optimizado ')
            pymonaco.calc_save(data)
            # Evaluamos la nueva cobertura:
            hyp_dict = read_hyp(hypfile)
            cv = evaluate_targets(hyp_dict)

        else:
            # RELAJAR ISOCONSTRAINTS SOLO 1 vez
            data['name'], plan_folder = duplicate_plan(data['clinic__folder__name'], data['clinic__name'],
                                                       data['nhist'], data['name'])
            hypfile = os.path.join(plan_folder, data['nhist'] + '.hyp')
            loosen_cf(hypfile)
            logging.info('calculando plan relajando constraints ')
            pymonaco.calc_save(data)
            # Evaluamos la nueva cobertura:
            hyp_dict = read_hyp(hypfile)
            cv = evaluate_targets(hyp_dict)
    logging.info('Secuencia de flujo automático terminada')
