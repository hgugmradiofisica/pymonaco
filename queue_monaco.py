# -*- coding: utf-8 -*-
"""
Created on Fri May 18 11:59:10 2018

@author: R. Ayala
"""
import os, logging, datetime
from pymonaco.settings import STANDALONE

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
print(dname)
import pymonaco.pymonaco as pymonaco
import pymonaco.util as util
import pymonaco.autoplan as autoplan

#LOG FILE
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG) # or any other level
logger.addHandler(ch)

fh = logging.FileHandler(os.path.join(dname,'queue.log'))
fh.setLevel(logging.DEBUG) # or any level you want
logger.addHandler(fh)
#_________________________________________________________________________
logger.info('*********************************************************************')
logger.info('Sº DE DOSIMETRÍA Y RADIOPROTECCIÓN - H.G.U. GREGORIO MARAÑÓN - ******')
logger.info('\nCOLA DE TAREAS PARA MONACO *******************************************\n')
logger.info(datetime.datetime.now())
logger.info('Abriendo lista de tareas...')

try:
    data = util.get_worklist()['results']
    logger.info('Lista de tareas cargada')
    logger.info('%s tareas en cola', len(data))
except:
    logger.error('ERROR: No se ha podido recuperar la lista de tareas')
    
if 'data' in locals():    
    for n in range(len(data)):
        logger.info('%s | %s | %s | %s | autoflow: %s', data[n]['clinic__folder__name'],
                    data[n]['clinic__name'], data[n]['nhist'], data[n]['name'], data[n]['opt_cfs'])
    monaco = pymonaco.connect()
    if STANDALONE == False:
        pymonaco.authorize(monaco)
    for n in range(len(data)):
        try:
            logger.info('Tarea %s de %s:', n+1, len(data))
            if data[n]['opt_cfs'] == True:
                autoplan.autoflow(monaco, data[n])
            else:
                pymonaco.calc_save(monaco, data[n])
            util.send_signal(data[n]['id'], 'OK')
        except Exception as e:
            logger.error(e)
            logger.exception('Ha habido un error! ')
            util.send_signal(data[n]['id'], 'ERROR')
    pymonaco.close_monaco(monaco)        

logger.info('*********************************************************************')
logger.info('*********************************************************************')
logger.info('Cola de cálculo terminada')
logger.info(datetime.datetime.now())
logger.info('*********************************************************************')
logger.info('*********************************************************************')